package ru.nfirex.javatests.tests;

import java.util.HashMap;

public class ArrayTests {
	public static int[] ARRAY = {12,2,4,5,6,3,21,1,3,-25,12,100,6,-70,-20,-60,12};
	public static int[] ARRAY_2 = {31,-41,59,26,-53,58,97,-93,-23,84};
	public static int[] ARRAY_WITH_REPEATS_EXCLUDE_ONE = {1,2,3,4,5,6,7,8,9,123,9,8,1,2,7,6,5,4,3};

	protected static String arrayToString(int[] array) {
		final StringBuilder builder = new StringBuilder().append(array[0]);
		for (int i = 0; i < array.length; i ++) {
			builder.append(',').append(array[i]);
		}

		return builder.toString();
	}




	/**
	 * Find one item in array that have no repeats
	 * @param array - array with repeats and one single number
	 */
	public static void findNotRepeatedNumber(int[] array) {
		int answer = 0;
		for (int item: array) {
			answer ^= item;
		}

		System.out.println("");
		System.out.println("Find not repeated number Test:");
		final String pattern = "Number with no repeats of array [%s] is %s";
		System.out.println(String.format(pattern, arrayToString(array), answer));
	}




	public static void findLongestSubsetWithZeroSumm(int[] array) {
		class Temp {
			private final int startIndex;

			private int lastIndex;
			private int length;

			public Temp(int startIndex) {
				this.startIndex = startIndex;
			}

			public void setLastindex(int lastIndex) {
				this.lastIndex = lastIndex;
			
				length = lastIndex - startIndex + 1;
			}

			public int getLength() {
				return length;
			}
		}

		final HashMap<Integer, Temp> map = new HashMap<Integer, Temp>();

		int answer = -1;
		int count = 0;
		for(int i = 0; i < array.length; i ++) {
			count += array[i];

			final Temp tmp = map.get(count);
			if (tmp == null) {
				map.put(count, new Temp(i));
			} else {
				tmp.setLastindex(i);

				if (answer < tmp.getLength()) {
					answer = tmp.getLength();
				}
			}
		}

		System.out.println("");
		System.out.println("Find longest subset with summ zero Test:");
		final String pattern = "The longest subset with summ zero in array [%s] is %s";
		System.out.println(String.format(pattern, arrayToString(array), answer));
	}




	/**
	 * Find subset with maximum summ. If all numbers < 0 then print 0.
	 * @param array
	 */
	public static void printMaxSumm(int[] array) {
		int answer = 0;
		int tmpMax = 0;
		int tmpSumm = 0;

		for (int item: array) {
			tmpSumm = tmpMax + item;

			if (tmpSumm > 0) {
				tmpMax = tmpSumm;
			} else {
				tmpMax = 0;
			}

			if (tmpMax > answer) {
				answer = tmpMax;
			}
		}

		System.out.println("");
		System.out.println("Find max summ in array Test:");
		final String pattern = "Max summ in array [%s] is %s";
		System.out.println(String.format(pattern, arrayToString(array), answer));
	}
}