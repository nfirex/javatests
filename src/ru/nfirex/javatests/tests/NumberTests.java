package ru.nfirex.javatests.tests;

public class NumberTests {
	public static int NUMBER = 1234567890;

	/**
	 * Print inverted number (decimal)
	 * @param number - base number for revert
	 */
	public static void invertNumber(int number) {
		final int tmpNumber = number;

		int answer = 0;
		while (number > 0) {
			final int digit = number % 10;
			number = number / 10;
			answer = answer * 10;
			answer += digit;
		}

		System.out.println("");
		System.out.println("Invert Number Test:");
		final String pattern = "Inverted number for %s is %s";
		System.out.println(String.format(pattern, tmpNumber, answer));
	}
}