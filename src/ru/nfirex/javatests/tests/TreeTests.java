package ru.nfirex.javatests.tests;

import java.util.ArrayList;

import ru.nfirex.javatests.objects.NodeWithChildren;
import ru.nfirex.javatests.objects.NodeWithParent;

public class TreeTests {
	public static final NodeWithChildren F = new NodeWithChildren("F", null, null);
	public static final NodeWithChildren E = new NodeWithChildren("E", null, null);
	public static final NodeWithChildren D = new NodeWithChildren("D", null, null);
	public static final NodeWithChildren C = new NodeWithChildren("C", null, F);
	public static final NodeWithChildren B = new NodeWithChildren("B", D, E);
	public static final NodeWithChildren A = new NodeWithChildren("A", B, C);
	public static final NodeWithChildren TREE_WITH_CHILDREN = A;

	public static NodeWithParent A2 = new NodeWithParent("A", null);
	public static NodeWithParent B2 = new NodeWithParent("B", A2);
	public static NodeWithParent C2 = new NodeWithParent("C", A2);
	public static NodeWithParent D2 = new NodeWithParent("D", B2);
	public static NodeWithParent E2 = new NodeWithParent("E", D2);
	public static NodeWithParent F2 = new NodeWithParent("F", D2);
	public static NodeWithParent TREE_WITH_PARENT = A2;

	/**
	 * Print common parent of tree (nodes with parents)
	 * @param a - node with parent
	 * @param b - node with parent
	 */
	public static void printCommonParent(NodeWithParent a, NodeWithParent b) {
		final NodeWithParent tmpA = a;
		final NodeWithParent tmpB = b;

		final int aLevel = checkLevel(a, 0);
		final int bLevel = checkLevel(b, 0);

		if (bLevel > aLevel) {
			b = moveToParent(b, bLevel, aLevel);
		} else if (aLevel > bLevel) {
			a = moveToParent(a, aLevel, bLevel);
		}

		final NodeWithParent parent = getParent(a, b);

		System.out.println("");
		System.out.println("Common Parent Test:");
		final String pattern = "Common parent of nodes %s and %s is %s";
		System.out.println(String.format(pattern, tmpA, tmpB, parent));
	}

	protected static int checkLevel(NodeWithParent node, int level) {
		while (node.parent != null) {
			node = node.parent;
			level ++;
		}

		return level;
	}

	protected static NodeWithParent moveToParent(NodeWithParent node, int curretnLevel, int destinationLevel) {
		while (curretnLevel != destinationLevel) {
			node = node.parent;
			curretnLevel --;
		}

		return node;
	}

	protected static NodeWithParent getParent(NodeWithParent a, NodeWithParent b) {
		while (a != b) {
			a = a.parent;
			b = b.parent;
		}

		return a;
	}




	/**
	 * Print levels of tree
	 * @param root - root of tree (nodes with children)
	 */
	public static void printLevels(NodeWithChildren root) {
		final ArrayList<StringBuilder> levels = new ArrayList<StringBuilder>();
		printLevels(root, 0, levels);

		System.out.println("");
		System.out.println("Levels Test:");
		for (StringBuilder builder: levels) {
			System.out.println(builder.toString());
		}
	}

	protected static void printLevels(NodeWithChildren node, int level, ArrayList<StringBuilder> levels) {
		if (node == null) {
			return;
		}

		final StringBuilder builder;
		if (level < levels.size()) {
			builder = levels.get(level);
			builder.append(" - ");
		} else {
			builder = new StringBuilder();
			levels.add(builder);
		}

		builder.append(node.data);

		final int newLevel = level + 1;
		printLevels(node.left, newLevel, levels);
		printLevels(node.right, newLevel, levels);
	}




	/**
	 * Print paths of tree
	 * @param root - root of tree (nodes with children)
	 */
	public static void printPaths(NodeWithChildren root) {
		System.out.println("");
		System.out.println("Paths Test:");
		printPaths(root, new StringBuilder());
	}

	protected static void printPaths(NodeWithChildren node, StringBuilder builder) {
		if (builder.length() > 0) {
			builder.append(" -> ");
		}

		builder.append(node.data);

		if (node.left == null && node.right == null) {
			System.out.println(builder.toString());
		} else {
			final int length = builder.length();
			if (node.left != null) {
				printPaths(node.left, builder);
			}

			if (node.right != null) {
				builder.setLength(length);
				printPaths(node.right, builder);
			}
		}
	}
}