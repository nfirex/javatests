package ru.nfirex.javatests;

import ru.nfirex.javatests.tests.ArrayTests;
import ru.nfirex.javatests.tests.NumberTests;
import ru.nfirex.javatests.tests.TreeTests;

public class Tests {
	public static void main(String[] args) {
		// Number Tests
		NumberTests.invertNumber(NumberTests.NUMBER);

		// Array Tests
		ArrayTests.findNotRepeatedNumber(ArrayTests.ARRAY_WITH_REPEATS_EXCLUDE_ONE);
		ArrayTests.findLongestSubsetWithZeroSumm(ArrayTests.ARRAY);
		ArrayTests.printMaxSumm(ArrayTests.ARRAY_2);

		// Tree Tests
		TreeTests.printCommonParent(TreeTests.D2, TreeTests.F2);
		TreeTests.printLevels(TreeTests.TREE_WITH_CHILDREN);
		TreeTests.printPaths(TreeTests.TREE_WITH_CHILDREN);
	}
}