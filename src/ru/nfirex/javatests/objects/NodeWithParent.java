package ru.nfirex.javatests.objects;

public class NodeWithParent {
	public final String data;
	public final NodeWithParent parent;

	public NodeWithParent(String data, NodeWithParent parent) {
		this.data = data;
		this.parent = parent;
	}

	@Override
	public String toString() {
		return data;
	}
}