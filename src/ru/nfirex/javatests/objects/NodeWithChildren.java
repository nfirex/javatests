package ru.nfirex.javatests.objects;

public class NodeWithChildren {
	public final String data;
	public final NodeWithChildren left;
	public final NodeWithChildren right;

	public NodeWithChildren(String data, NodeWithChildren left, NodeWithChildren right) {
		this.data = data;
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return data;
	}
}